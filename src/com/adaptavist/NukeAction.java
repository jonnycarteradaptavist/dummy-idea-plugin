package com.adaptavist;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.IconLoader;

public class NukeAction extends AnAction {
    public NukeAction() {
        super("Nuke it", "Take off and nuke it from orbit", IconLoader.getIcon("/resources/icons/nuke/nuke32.png"));
    }

    public void actionPerformed(AnActionEvent event) {
        Project project = event.getData(PlatformDataKeys.PROJECT);
        String txt= Messages.showInputDialog(project, "Identify planet:", "Input planet name", Messages.getQuestionIcon());
        Messages.showMessageDialog(project, "Tactical nukes deployed against " + txt + ".", "Information", Messages.getInformationIcon());
    }
}
